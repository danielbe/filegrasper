﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FileGrasper
{
    class LocalFile
    {
        private List<string> urls;

        internal LocalFile(List<string> urls)
        {
            this.urls = urls;
        }

        internal async Task Download()
        {
            StringBuilder sb = new StringBuilder();
            int current = 1, total = urls.Count;

            foreach (string url in urls)
            {
                Console.WriteLine($"Downloading file {current} of {total} ({url})");

                //todo: what if / is the last character in url?
                string fileName = url.Substring(url.LastIndexOf("/") + 1);
                FileInfo info = new FileInfo(fileName);
                current++;

                if (info.Exists && info.Length > 0)
                {
                    Console.WriteLine("Already downloaded");
                    continue;
                }

                await FileGrasper.Download.Get(url);

                Console.WriteLine("Success");
                sb.Append("Success - ");
            }
        }
    }
}
