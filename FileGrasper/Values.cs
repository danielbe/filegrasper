﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileGrasper
{
    class Values
    {
        internal static readonly List<string> Audio = new List<string>() { "3gp", "aac", "flac", 
            "m4a", "ogg", "wma", "mp3", "wav" };
        internal static readonly List<string> AudioAll = new List<string>() { "3gp", "aa", "aac", 
            "aax", "act", "aiff", "alac", "amr", "ape", "au", "awb", "dct", "dss", "dvf", "flac", 
            "gsm", "iklax", "ivs", "m4a", "m4b", "m4p", "mmf", "mp3", "mpc", "msv", "nmf", "ogg", 
            "oga", "mogg", "opus", "ra", "rm", "raw", "rf64", "sln", "tta", "voc", "vox", "wav", 
            "wma", "wv", "webm", "8svx", "cda" };
        internal static readonly List<string> Video = new List<string>() { "webm", "mkv", "flv",
            "avi", "ts", "mov", "wmv", "asf", "mpeg", "mpg", "m4v", "3gp", "ogv", "mp4" };
        internal static readonly List<string> VideoAll = new List<string>() { "webm", "mkv", "flv",
            "avi", "ts", "mov", "wmv", "asf", "mpeg", "mpg", "3gp", "vob", "ogv", "drc", 
            "gif", "gifv", "mng", "mts", "m2ts", "qt", "yuv", "rm", "rmvb", "viv", "amv", "mp4",
            "m4p", "m4v", "mp2", "mpe", "mpv", "m2v", "m4v", "svi", "3g2", "mxf", "roq", "nsv",
            "f4v", "f4p", "f4a", "f4b" };

        internal static List<string> GetList(string key)
        {
            if (key.ToLower().Equals("audio"))
                return Audio;
            else if (key.ToLower().Equals("audioall"))
                return AudioAll;
            else if (key.ToLower().Equals("video"))
                return Video;
            else if (key.ToLower().Equals("videoall"))
                return VideoAll;
            else
                return null;
        }

        internal static readonly string RegexLocal = "^[a-zA-Z]:";
    }
}
