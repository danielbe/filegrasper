﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FileGrasper
{
    class Download
    {
        private static HttpClient client = new HttpClient();

        internal static async Task Get(string url, string fileName = "")
        {
            if (string.Empty.Equals(fileName))
            {
                FileInfo info = new FileInfo(url);
                fileName = info.Name;
            }

            try
            {

                HttpResponseMessage response =
                    await client.GetAsync(url, HttpCompletionOption.ResponseHeadersRead);

                response.EnsureSuccessStatusCode();

                Stream content = await response.Content.ReadAsStreamAsync();

                using (var fileStream = File.Create(fileName))
                {
                    //content.Seek(0, SeekOrigin.Begin);
                    content.CopyTo(fileStream);
                }
            }
            catch (HttpRequestException e)
            {
                Console.Write("ERROR - ");
                Console.WriteLine(url);
                Console.WriteLine(e.Message);
            }
        }
    }
}
