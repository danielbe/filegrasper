﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FileGrasper
{
    class Program
    {
        static void Main(string[] args)
        {
            //todo: not having to use specific order of arguments
            //-e mp3 -r 100k
            if (args.Length > 1)
            {
                string fileName = args[0];

                List<string> extensions = new List<string>();
                //add all audio types if audio specified
                if (args[1].ToLower().Equals("help"))
                {
                    Help.Display();
                }
                else 
                {
                    extensions = Values.GetList(args[1]);

                    if (null == extensions)
                    {
                        //add extensions to list
                        for (int i = 1; i < args.Length; i++)
                            extensions.Add(args[i]);
                    }
                }

                //todo not only match http but also 10.292. etc.
                //online feed. needs to be downloaded first
                if (args[0].StartsWith("http"))
                {
                    fileName = OnlineFile.Get(args[0]).Result;
                }

                FileInteraction fi = new FileInteraction(fileName, extensions);
                //FileInteraction fi = new FileInteraction(@"C:\Users\mahuu\Downloads\podcast-ufo.fail.txt", "mp3");
                List<string> urls = fi.Read();
                //urls.ForEach(u => Console.WriteLine(u));
                LocalFile lf = new LocalFile(urls);
                lf.Download().Wait();

                Console.ReadKey();
            }
            else
            {
                Help.Display();
                Console.ReadKey();
            }
        }
    }
}
