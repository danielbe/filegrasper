﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FileGrasper
{
    /// <summary>
    /// downloads feed from given url
    /// </summary>
    class OnlineFile
    {
        private static readonly string fileName = "feed.txt";

        internal static async Task<string> Get(string url)
        {
            await Download.Get(url, fileName);
            return fileName;
        }
    }
}
