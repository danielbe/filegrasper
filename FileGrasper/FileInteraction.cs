﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace FileGrasper
{
    class FileInteraction
    {
        private string file = "";
        private string regexUrl = "";

        internal FileInteraction(string file, List<string> extensions)
        {
            this.file = file;
            string extension = string.Join('|', extensions);
            regexUrl = @"http[s]*:\/\/.+[.](" + extension + ")";
        }


        internal List<string> Read()
        {
            string line = "";
            List<string> urls = new List<string>();

            try
            {
                StreamReader reader = new StreamReader(file);
                Regex regex = new Regex(regexUrl, RegexOptions.IgnoreCase);

                while ((line = reader.ReadLine()) != null)
                {
                    MatchCollection matches = regex.Matches(line);
                    foreach (Match match in matches)
                    {
                        urls.Add(match.Value);
                    }
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error reading from file");
                Console.WriteLine(ex.Message);
            }

            return urls;
        }
    }
}
