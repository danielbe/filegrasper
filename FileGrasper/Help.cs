﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileGrasper
{
    class Help
    {
        internal static void Display()
        {
            Console.WriteLine("Usage is as follows:");
            Console.WriteLine("feed fileExtension [fileExtension]");
            Console.WriteLine(@"c:\feed.xml mp3 wav");
            Console.WriteLine("Instead of an extension you can type audio");
            Console.WriteLine(@"c:\feed.xml audio");
            Console.WriteLine(@"http://google.com/podcast mp4 mp3 mp2 mp1");

            Console.WriteLine("Instead of an audio you can also typing the following:");
            Console.WriteLine("audioAll, video, videoAll");
        }
    }
}
